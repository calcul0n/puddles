-- api.lua
--

local BIGNUM = 1000000

puddles.settings = {}
puddles.registered_regions = {}
puddles.registered_decorations = {}
puddles._decoration_id = 0

puddles.default_bottom = "default:sand"
-- puddles.default_shore = "default:dirt_with_grass"

puddles.default_remove_nodes = {
   "group:tree",

   -- [fixme] could have an ignore list instead to keep stuff like
   -- mushrooms entirely in water

   -- pore and caps use leafdecay
   "ethereal:mushroom_trunk",
   -- "ethereal:mushroom",
   -- "ethereal:mushroom_pore",
}
puddles.default_forbidden_contents = {
   "group:liquid",
}
puddles.default_forbidden_grounds = {
   "group:leaves",
}


function puddles.register_region (def)
   def = table.copy(def)

   assert(def.name)
   assert(not puddles.registered_regions[def.name], def.name)

   puddles.registered_regions[def.name] = def

   -- position
   local min_pos = def.min_pos or {}
   min_pos.x = min_pos.x or -32000
   min_pos.y = min_pos.y or def.y_min or -32000
   min_pos.z = min_pos.z or -32000
   def.min_pos = min_pos

   local max_pos = def.max_pos or {}
   max_pos.x = max_pos.x or 32000
   max_pos.y = max_pos.y or def.y_max or 32000
   max_pos.z = max_pos.z or 32000
   def.max_pos = max_pos

   -- nodes
   def.fill_node = def.fill_node or "default:river_water_source"
   def.fill_frozen_node = def.fill_frozen_node or "default:ice"

   -- chance/limits
   def.chance = def.chance or 100
   def.min_depth = def.min_depth or 0
   def.max_depth = def.max_depth or BIGNUM
   def.min_surface = def.min_surface or 0
   def.max_surface = def.max_surface or BIGNUM
   def.min_capacity = def.min_capacity or 0
   def.max_capacity = def.max_capacity or BIGNUM

   -- freeze/evaporate
   def.freeze_heat = def.freeze_heat or 20
end


function puddles.register_decoration (def)
   def = table.copy(def)

   def.id = puddles._decoration_id + 1
   puddles._decoration_id = def.id
   puddles.registered_decorations[def.id] = def
   
   def.type = def.type or "simple"
   def.where = def.where or "shore"
   if def.regions == nil then
      -- all regions
   elseif type(def.regions) == "string" then
      def.regions = {def.regions}
   else
      assert(type(def.regions) == "table")
   end
   def.heat_min = def.heat_min or -1000
   def.heat_max = def.heat_max or 1000
   def.humidity_min = def.humidity_min or -1000
   def.humidity_max = def.humidity_max or 1000
   -- [todo] biomes
end


-- [todo] disable setting
puddles.register_region({
      name = "default",
      y_min = 2,
      y_max = 32000,
      shore_decoration_chance = 4,
})

-- temperate
puddles.register_decoration({
      region = "default",
      type = "simple",
      decoration = {
         "default:grass_1",
         "default:grass_2",
         "default:grass_3",
         "default:grass_4",
         "default:grass_5",
      },
      heat_min = 25,
      heat_max = 75,
})

-- hot
puddles.register_decoration({
      region = "default",
      type = "simple",
      decoration = {
         "default:dry_shrub",
         "default:dry_grass_1",
         "default:dry_grass_2",
         "default:dry_grass_3",
         "default:dry_grass_4",
         "default:dry_grass_5",
         "default:junglegrass",
         -- [fixme] marram_grass doesn't look that good on green
         -- shores
         "default:marram_grass_1",
         "default:marram_grass_2",
         "default:marram_grass_3",
      },
      heat_min = 70,
})

-- cold
puddles.register_decoration({
      region = "default",
      type = "simple",
      decoration = {
         "default:fern_1",
         "default:fern_2",
         "default:fern_3",
      },
      heat_max = 40,
})

local dpath = minetest.get_modpath("default").."/schematics/"
local dschems = {
   -- name             heat_min heat_max offsy
   { "bush",           nil,     75,      -1 },
   { "acacia_bush",    60,      nil,     -1 },
   { "blueberry_bush", 40,      75,      nil },
}
for _, dec in ipairs(dschems) do
   puddles.register_decoration({
         region = "default",
         where = "shore",
         type = "schematic",
         schematic = dpath..dec[1]..".mts",
         flags = "place_center_x,place_center_z",
         heat_min = dec[2],
         heat_max = dec[3],
         place_offset_y = dec[4],
   })
end

-- papyrus (make it smaller and with no ground)
local function papyrus_mts ()
   local P = {name="default:papyrus", prob=255}
   return {
      size={x=1, y=4, z=1},
      yslice_prob = {
         {ypos=2, prob=63},
         {ypos=3, prob=63},
         {ypos=4, prob=63},
      },
      data = {P, P, P, P},
   }         
end

puddles.register_decoration({
      region = "default",
      where = "shore",
      type = "schematic",
      schematic = papyrus_mts(),
      -- place_offset_y = -2,
      rarity = 2,
      heat_min = 60,
})

puddles.register_decoration({
      region = "default",
      where = "surface",
      type = "simple",
      decoration = "flowers:waterlily",
      heat_min = 40,
})

if farming and farming.mod == "redo" then
   -- [TODO] add more but maybe not all crops, they wouldn't all make
   -- sense around a lake i guess
   puddles.register_decoration({
         region = "default",
         where = "shore",
         type = "simple",
         decoration = "farming:melon_8",
         rarity = 3,
         heat_min = 60,
         humidity_min = 60,
   })
end

if minetest.get_modpath("ethereal") then
   local epath = minetest.get_modpath("ethereal").."/schematics/"   
   local eschems = {
      -- name             heat_min heat_max rarity
      { "bush",           30,      nil,     3 },
      { "bambootree",     50,      nil,     4 },
      { "vinetree",       60,      nil,     4 },
   }
   for _, dec in ipairs(eschems) do
      puddles.register_decoration({
            region = "default",
            where = "shore",
            type = "schematic",
            schematic = ethereal[dec[1]],
            flags = "place_center_x,place_center_z",
            heat_min = dec[2],
            heat_max = dec[3],
            rarity = dec[4],
      })
   end
end
