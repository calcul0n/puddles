-- region.lua
--

local MODNAME = minetest.get_current_modname()
local LOGGER = puddles.mtt.logger

local ldump = puddles.mtt.ldump
local dump_table = puddles.mtt.ldump


-- select_group_nodes
--
local function select_group_nodes (group)
   local res = {}
   for name, def in pairs(minetest.registered_nodes) do
      local l = minetest.get_item_group(name, group)
      if l > 0 then
         table.insert(res, name)
      end
   end
   return res
end


-- add_nodes
--
local function add_node (tbl, name)
   if minetest.registered_nodes[name] then
      tbl[minetest.get_content_id(name)] = name
   else
      LOGGER:debug("ignored node: %s", name)
   end
end


-- fix_nodes_list
--
local function fix_nodes_list (nodes)
   local fix = {}
   for _, spec in ipairs(nodes) do
      local mod, name = unpack(spec:split(":"))
      if mod == "group" then
         for _, n in ipairs(select_group_nodes(name)) do
            add_node(fix, n)
         end
      else
         add_node(fix, spec)
      end
   end
   return fix
end


-- match_list
--
local function match_list (list, name)
   if list == nil then
      return true
   elseif type(list) == "string" then
      return list == name
   else
      for _, n in ipairs(list) do
         if n == name then
            return true
         end
      end
   end
   return false
end


-- get_id_list
--
local function get_id_list (list)
   local id
   local rlist = {}
   if type(list) == "string" then
      id = minetest.get_content_id(list)
      if id then
         rlist = {id}
      else
         LOGGER:error("unknown node: %s", list)
      end
   elseif type(list) == "table" then
      for _, name in ipairs(list) do
         id = minetest.get_content_id(name)
         if id then
            table.insert(rlist, id)
         else
            LOGGER:error("unknown node: %s", name)
         end
      end
   else
      LOGGER:error("invalid list type: %s", tostring(list))
   end
   return rlist
end


-- init_decoration
--
local function init_decoration (deco)
   if deco.type == "simple" then
      deco.decoration = get_id_list(deco.decoration)
      if #deco.decoration == 0 then
         return false
      end
      deco.rarity = deco.rarity or 1
   elseif deco.type == "schematic" then
      -- check if existing?
      deco.rarity = deco.rarity or 3
   else
      LOGGER:error("invalid decoration type: %s", deco.type)
      return false
   end
   return true
end


-- init_region_decorations
--
local function init_region_decorations (region)
   region.shore_decorations = {}
   region.surface_decorations = {}
   for id, def in pairs(puddles.registered_decorations) do
      if match_list(def.regions, region.name) then
         if def.where == "shore" then
            region.shore_decorations[id] = def
         elseif def.where == "surface" then
            region.surface_decorations[id]= def
         else
            LOGGER:error("invalid 'where' attribute: '%s'", def.where)
         end
      end
   end
end


-- init_region
--
local function init_region (region)
   LOGGER:debug("initializing region %s", region.name)
   
   local bottom = region.bottom or puddles.default_bottom
   assert(minetest.registered_nodes[bottom], bottom)
   region.bottom = minetest.get_content_id(bottom)

   region.fill_id = minetest.get_content_id(region.fill_node)
   region.fill_frozen_id = minetest.get_content_id(region.fill_frozen_node)
   
   local remove_nodes = {}
   for id, name in pairs(fix_nodes_list(region.remove_nodes or puddles.default_remove_nodes)) do
      remove_nodes[id] = true
   end
   region.remove_nodes = remove_nodes

   region.forbidden_contents = fix_nodes_list(region.forbidden_contents or puddles.default_forbidden_contents)

   region.forbidden_grounds = fix_nodes_list(region.forbidden_grounds or puddles.default_forbidden_grounds)

   init_region_decorations(region)
   
   LOGGER:debug("region initialized: %s", region.name)
   LOGGER:debug("bottom: %d", region.bottom)
   LOGGER:debug(dump_table("remove_nodes:", region.remove_nodes))
   LOGGER:debug(dump_table("forbidden contents:", region.forbidden_contents))
   LOGGER:debug(dump_table("forbidden grounds:", region.forbidden_grounds))
   LOGGER:debug(dump_table("shore_decorations:", region.shore_decorations))
   LOGGER:debug(dump_table("surface_decorations:", region.surface_decorations))
end


-- puddles._init
--
-- called at generation time
--
local init_done = false
function puddles._init ()
   if init_done then
      return
   end
   init_done = true

   -- decorations
   for id, def in pairs(puddles.registered_decorations) do
      if not init_decoration(def) then
         LOGGER:warning("removing invalid decoration: %s", ldump(def))
         puddles.registered_decorations[id] = nil
      end
   end
   
   -- regions
   for _, def in pairs(puddles.registered_regions) do
      init_region(def)
   end
end
