
local MODNAME = minetest.get_current_modname()

local LOGGER = puddles.mtt.logger

local ENABLE_DEBUG = true
local ENABLE_DEBUG_CHUNKS = true
local ENABLE_DEBUG_HEIGHTMAP = false
local ENABLE_DEBUG_SIGNS = true

local P2S = minetest.pos_to_string

local BIGNUM = 1000000
local random = math.random
local min = math.min
local max = math.max
local floor = math.floor
local function clamp (v, mi, ma)
   return max(min(v, ma), mi)
end
local function round (v)
   return floor(v + 0.5)
end


-- globals
--
local content
local data = {}
local param2_data = {}
local minp, maxp, chulen
local vm, area
local after_destruct_handlers
local heightmap
local env
local schematics_list


--
local neighbours = {
   { 0, -1},
   { 0,  1},
   { 1,  0},
   {-1,  0},
}


local content_list =
   {
      air = "air",
      mark_ground = "default:goldblock",
      mark_border = "default:obsidian",
      water = "default:river_water_source", --"wool:blue",
      floor = "default:cobble",
      mud = "default:clay",
   }


-- get_content
--
local function get_content ()
   if content == nil then
      content = {}
      for k, n in pairs(content_list) do
         assert(minetest.registered_nodes[n], k.."="..n)
         content[k] = minetest.get_content_id(n)
      end
   end
   content.shores = {}
   for i, col in ipairs({
         "white",
         "yellow",
         "grey",
         "green",
         "dark_grey",
         "black",
                       })
   do
      content.shores[i] = minetest.get_content_id("wool:"..col)
   end
   return content
end


local function rect_intersects (min1, max1, min2, max2)
   return min1.x <= max2.x and max1.x >= min2.x
      and min1.y <= max2.y and max1.y >= min2.y
      and min1.z <= max2.z and max1.z >= min2.z
end


-- select_decoration
--
local function select_decoration (env, deco)
   if env.heat < deco.heat_min or env.heat > deco.heat_max then
      return false
   end
   return true
end


-- get_environment_info
--
local function get_environment_info (minp, maxp)
   local env = {}

   -- find region
   local found_regions = {}
   for _, region in pairs(puddles.registered_regions) do
      if rect_intersects(minp, maxp, region.min_pos, region.max_pos) then
         table.insert(found_regions, region)
      end
   end
   local region
   if #found_regions == 1 then
      region = found_regions[1]
   elseif #found_regions > 1 then
      region = found_regions[math.random(#found_regions)]
   end
   env.region = region

   -- pick biome and average heat/humidity
   local biomemap = minetest.get_mapgen_object("biomemap")
   local heatmap = minetest.get_mapgen_object("heatmap")
   local humiditymap = minetest.get_mapgen_object("humiditymap")
   env.biome_id = biomemap and biomemap[math.floor(#biomemap/2)] or 0
   env.heat = heatmap and heatmap[math.floor(#heatmap/2)] or 50
   env.humidity = humiditymap and humiditymap[math.floor(#humiditymap/2)] or 50

   -- select decorations
   if region then
      local shore_decorations = {{}, {}, {}, {}}
      local surface_decorations = {}
      env.shore_decorations = shore_decorations
      env.surface_decorations = surface_decorations
      for _, def in pairs(region.shore_decorations) do
         if select_decoration(env, def) then
            table.insert(shore_decorations[def.rarity], def)
         end
      end
      for _, def in pairs(region.surface_decorations) do
         if select_decoration(env, def) then
            table.insert(surface_decorations, def)
         end
      end
   end

   -- chance and limits
   --
   -- hot env makes chance lower
   -- dry env makes lakes smaller ([fixme] depth/volume/surface?)
   --
   env.chance = clamp((100.0 - env.heat) * 1.286 + 10.0, 10.0, 100.0)
   env.max_depth = round(math.pow(env.humidity, 3.0) * 9.25926e-05 + 1.0)
   local evaporate_chance = env.heat * 0.45 + 5.0
   if random(100) < evaporate_chance then
      env.evaporate = random(round(env.heat*0.02)+1)
   else
      env.evaporate = 0
   end
   if env.region then
      env.min_depth = region.min_depth
      env.min_surface = region.min_surface
      env.min_capacity = region.min_capacity
      env.max_capacity = region.max_capacity
      env.min_depth = region.min_depth
      env.max_depth = min(env.max_depth, region.max_depth)
      env.chance = env.chance * region.chance / 100.0
      env.evaporate_chance = env.heat * 0.9 + 10.0
   else
      env.min_depth = 0
      env.min_surface = 0
      env.min_capacity = 0
      env.max_capacity = BIGNUM
      env.min_depth = 0
   end

   return env
end


-- debug_heightmap
--
local function debug_heightmap ()
   local c = get_content()
   local idx = 1
   for z = minp.z, maxp.z do
      for x = minp.x, maxp.x do
         local y = heightmap[idx]
         data[area:index(x, y, z)] = c.mark_ground
         idx = idx + 1
      end
   end
end


-- debug_chunks
--
local function debug_chunks ()
   local c = get_content()
   local idx = 1
   for z = minp.z, maxp.z do
      local bz = (z == minp.z)
      for x = minp.x, maxp.x do
         local bx = (x == minp.x)
         local height = heightmap[idx]
         if bx or bz then
            if height >= minp.y and height <= maxp.y then
               data[area:index(x, height, z)] = c.mark_border
            end
         end
         -- for y = minp.y, maxp.y do
         --    local by = (y == minp.y)
         --    if (bx and by) or (bx and bz) or (by and bz) then
         --       data[area:index(x, y, z)] = c.mark_border
         --    end
         -- end
         idx = idx + 1
      end
   end
end


-- -- debug_mark_bases
-- --
-- local function debug_mark_bases (bases)
--    local c = get_content()
--    for _, base in ipairs(bases) do
--       local y = base.height
--       local bot = c.bottoms[(base.id % #c.bottoms) + 1]
--       for _, node in ipairs(base.nodes) do
--          data[area:index(node.x, y, node.z)] = bot
--       end
--    end
-- end


-- helpers
--
local function is_in_chunk (x, z)
   return x >= minp.x and x <= maxp.x and z >= minp.z and z <= maxp.z
end

local function is_border (x, z)
   return x == minp.x or x == maxp.x or z == minp.z or z == maxp.z
end


-- shore_size
--
local function shore_size (surface)
   return math.max(1, math.min(6,  math.floor((surface + 80) / 100)))
end


-- shore_chance
--
-- percent chances to get a shore at dist for a given size - [fixme]
-- could be a lookup table
--
local function shore_chance (size, dist)
   -- [todo]
   return (110.0 - (dist * 10))
end


-- detect_base
--
local function detect_base (x, z, index, done)
   local forbidden_grounds = env.region.forbidden_grounds
   local forbidden_contents = env.region.forbidden_contents
   local height = heightmap[index]
   -- note: test >= because we can't place water if height==maxp.y
   if height < minp.y or height >= maxp.y then
      return nil
   end
   -- [FIXME] check all area
   if height < env.region.min_pos.y then
      base_node = nil
   end
   local todo = {{x=x, y=height, z=z, index=index}}
   done[index] = true
   local base_nodes = {}
   while #todo > 0 do
      local node = table.remove(todo)
      -- LOGGER:debug("node: {%d,%d}", node.x, node.z)
      if is_border(node.x, node.z) then
         -- can't be a base
         base_nodes = nil
      end
      -- check forbidden contents and grounds [fixme] should we rather
      -- check all content when detecting volumes? (seems expensive
      -- and not that useful)
      local ground = data[area:index(node.x, height, node.z)]
      local content = data[area:index(node.x, height+1, node.z)]
      if forbidden_grounds[ground] or forbidden_contents[content] then
         base_nodes = nil
      end
      if base_nodes then
         table.insert(base_nodes, node)
      end
      for _, n in ipairs(neighbours) do
         local nx = node.x + n[1]
         local nz = node.z + n[2]
         if is_in_chunk(nx, nz) then
            local ni = node.index + n[2] * chulen + n[1]
            -- LOGGER:debug("neighbour: {%d,%d} -> {%d,%d} -> %d", n[1], n[2], nx, nz, ni)
            local ny = heightmap[ni]
            if ny < height then
               -- a neighbour is lower, can't be a base
               base_nodes = nil
            elseif ny == height and not done[ni] then
               table.insert(todo, {x=nx, y=height, z=nz, index=ni})
               done[ni] = true
            end
         end
      end
   end
   if base_nodes then
      return {height=height, nodes=base_nodes}
   else
      return nil
   end
end


-- detect_all_bases
--
local function detect_all_bases (gen)
   local base_list = {}
   local done = {}
   local index = 1
   for z = minp.z, maxp.z do
      for x = minp.x, maxp.x do
         if not done[index] then
            local base = detect_base(x, z, index, done)
            if base then
               -- skip by chance
               if env.chance > 0 and (env.chance >= 100 or random(100) < env.chance) then
                  -- note: only for debug
                  base.id = #base_list
                  table.insert(base_list, base)
               else
                  LOGGER:debug("base skipped")
               end
            end
         end
         index = index + 1
      end
   end
   gen.bases = base_list
   return base_list
end


-- merge_volumes
--
local function merge_volumes (volumes, id1, id2)
   LOGGER:debug("merge volumes: %d <- %d", id1, id2)

   local vol1 = volumes[id1]
   assert(vol1, id1)
   local vol2 = volumes[id2]
   assert(vol2, id2)

   local vol1_bases = vol1.bases
   for _, b in ipairs(vol2.bases) do
      table.insert(vol1_bases, b)
   end
   local vol1_nodes = vol1.nodes
   for _, n in ipairs(vol2.nodes) do
      table.insert(vol1_nodes, n)
      n.volume = id1
   end

   -- note: surface count is not handled here
   vol1.depth = math.max(vol1.depth, vol2.depth)
   vol1.capacity = vol1.capacity + vol2.capacity
   
   -- drop vol2
   volumes[id2] = nil
end


-- detect_volume_level
--
local function detect_volume_level (height, vol, volumes, fillmap)
   local vol_id = vol.id
   local base_node = vol.bases[1].nodes[1]
   local start_node = {x=base_node.x, y=height, z=base_node.z, index=base_node.index, volume=vol_id}
   local todo = {start_node}
   local done = {[start_node.index] = start_node}
   local count = 1
   local volmerge = {}
   while #todo > 0 do
      local node = table.remove(todo)
      for _, n in ipairs(neighbours) do
         local nx = node.x + n[1]
         local nz = node.z + n[2]
         local ni = node.index + n[2] * chulen + n[1]
         if is_in_chunk(nx, nz) and not done[ni] then
            local ny = heightmap[ni]
            if ny >= height then
               -- ground will block water, good
            elseif is_border(nx, nz) then
               -- will flow outside of chunk, skip level
               return nil
            elseif ny == (height-1) then
               -- just above ground, go on
               local ngb = {x=nx, y=height, z=nz, index=ni, volume=vol_id}
               table.insert(todo, ngb)
               done[ni] = ngb
               count = count + 1
            else
               -- there's a hole, only fill if we can merge another volume
               local fillnode = fillmap[ni]
               if fillnode and fillnode.y == (height-1) then
                  local ngb = {x=nx, y=height, z=nz, index=ni, volume=vol_id}
                  table.insert(todo, ngb)
                  done[ni] = ngb
                  count = count + 1
                  if fillnode.volume ~= vol_id then
                     volmerge[fillnode.volume] = true
                  end
               else
                  return nil
               end
            end
         end
      end
   end
   -- warning: remember done is not a list!
   return done, count, volmerge
end


-- detect_volume
--
local function detect_volume (vol, volumes, fillmap)
   -- note: volume should have only one base at this stage
   assert(#vol.bases == 1)
   local base = vol.bases[1]
   local vol_id = vol.id
   -- [fixme] could quickly (and safely) fill level 1
   for height = base.height + 1, maxp.y do
      if vol.depth >= env.max_depth then
         return
      end
      local level_nodes, level_count, volmerge = detect_volume_level(height, vol, volumes, fillmap)
      if level_nodes then
         -- merge volumes if possible
         local total_capa = vol.capacity + level_count
         for mid, _ in pairs(volmerge) do
            local mvol = volumes[mid]
            total_capa = total_capa + mvol.capacity
            if total_capa > env.max_capacity then
               -- too big, stop here
               return
            end
            if mvol.depth >= env.max_depth then
               -- too deep
               return
            end
         end
         -- note: keep merge params in that order! (so we drop the
         -- volume for which detection is done)
         for mid, _ in pairs(volmerge) do
            merge_volumes(volumes, vol_id, mid)
         end
         --
         vol.height = height
         -- local vol_id = vol.id
         local vol_nodes = vol.nodes
         -- warning: remember it's not a list!
         -- local surface_count = 0 -- [fixme] remove?
         for i, n in pairs(level_nodes) do
            table.insert(vol_nodes, n)
            fillmap[i] = n
         end
         vol.surface = level_count
         vol.capacity = vol.capacity + level_count
         vol.depth = vol.depth + 1
      else
         break
      end
   end
end


-- detect_all_volumes
--
local function detect_all_volumes (gen)
   local volumes = {}
   for id, b in ipairs(gen.bases) do
      volumes[id] = {
         id = id,
         bases = {b},
         nodes = {},
         depth = 0,
         capacity = 0,
         surface = 0,
      }
   end

   local fillmap = {}
   -- copy volumes so we can remove volumes while iterating
   local itvol = {}
   for k, v in pairs(volumes) do
      itvol[k] = v
   end
   for id, vol in pairs(itvol) do
      detect_volume(vol, volumes, fillmap)
   end
   -- [TODO] drop volumes which are too small (and take care of
   -- fillmap!)
   gen.volumes = volumes
   gen.surface = fillmap
   return volumes, fillmap
end


local shore_neighbours_cache = {}
local function shore_neighbours (width)
   local tbl = shore_neighbours_cache[width]
   if tbl == nil then
      tbl = {}
      for j = -width, width do
         for i = -width, width do
            if not (i==0 and j==0) then
               table.insert(tbl, {i, j})
            end
         end
      end
      shore_neighbours_cache[width] = tbl
   end
   return tbl
end


local walkable_nodes_cache = {}
local function is_walkable (id)
   local val = walkable_nodes_cache[id]
   if val == nil then
      local name = minetest.get_name_from_content_id(id)
      local def = minetest.registered_nodes[name]
      assert(def, name)
      val = def.walkable
      walkable_nodes_cache[id] = val
   end
   return val
end


local function detect_shore_at (x, y, z, index, dist, shore_size, shores)
   local node = shores[index]
   if node then
      node.dist = math.min(dist, node.dist)
      node.shore_size = math.max(shore_size, node.shore_size)
      return
   end
   for sy = y, math.min(y+2, (maxp.y-1)) do
      if not is_walkable(data[area:index(x, sy+1, z)]) then
         shores[index] = {x=x, y=sy, z=z, index=index, dist=dist, shore_size=shore_size}
         return
      end
   end
end


-- detect_shores
--
-- [fixme] very ineficient, find better?
--
local function detect_shores (gen)
   for _, vol in pairs(gen.volumes) do
      vol.shore_size = shore_size(vol.surface)
   end
   
   local fillmap = gen.surface
   local shores = {}
   for index, node in pairs(fillmap) do
      -- [fixme] having a per-volume surface list would help a lot
      -- here
      local shore_size = gen.volumes[node.volume].shore_size
      for _, n in ipairs(shore_neighbours(shore_size)) do
         local nx = node.x + n[1]
         local nz = node.z + n[2]
         if is_in_chunk(nx, nz) then
            local ni = node.index + n[2] * chulen + n[1]
            if not fillmap[ni] then
               local dist = math.abs(n[1])+math.abs(n[2])
               if dist <= shore_size then
                  detect_shore_at(nx, node.y, nz, ni, dist, shore_size, shores)
               end
            end
         end
      end
   end
   -- random filter
   for i, n in pairs(shores) do
      if math.random(100) >= shore_chance(n.shore_size, n.dist) then
         shores[i] = nil
      end
   end
   gen.shores = shores
   return shores
end


-- get_after_destruct
--
local after_destruct_cache = {}
local function get_after_destruct (id)
   local val = after_destruct_cache[id]
   if val == nil then
      local name = minetest.get_name_from_content_id(id)
      assert(name, id)
      local def = minetest.registered_nodes[name]
      assert(def, name)
      if def.after_destruct then
         val = {def.after_destruct, name}
         after_destruct_cache[id] = val
      else
         after_destruct_cache[id] = false
      end
   end
   if val then
      return unpack(val)
   else
      return nil
   end
end


-- clear_node
--
local function replace_node (pos, id)
   local index = area:indexp(pos)
   local old_id = data[index]
   local dto, name = get_after_destruct(old_id)
   if dto then
      table.insert(after_destruct_handlers,
                   {dto, pos,
                    {name=name, param2=param2_data[index]}})
   end
   data[index] = id
end


-- fill_all_volumes
--
local function fill_all_volumes (volumes, fillmap)
   local region = env.region
   assert(region)
   
   local c = get_content() -- remove me

   local fill_id
   if env.heat > region.freeze_heat then
      fill_id = region.fill_id
   else
      fill_id = region.fill_frozen_id
   end

   -- -- could use fillmap and go from n.y to bed
   -- for _, vol in pairs(volumes) do
   --    for _, node in ipairs(vol.nodes) do
   --       data[area:index(node.x, node.y, node.z)] = fill_id
   --    end
   -- end

   -- clear surface
   local remove_nodes = env.region.remove_nodes
   local bottom = env.region.bottom

   for i, node in pairs(fillmap) do
      local pos = {x=node.x, z=node.z}
      local y_fill = node.y
      local y_surface = y_fill - env.evaporate
      local y_bed = heightmap[i]
      -- clear evaporated area (remove all)
      for y = y_surface+1, y_fill do
         pos.y = y
         replace_node(pos, c.air)
      end
      -- clear nodes above surface (remove_nodes only)
      for y = y_fill+1, maxp.y do
         pos.y = y
         if remove_nodes[data[area:indexp(pos)]] then
            replace_node(pos, c.air)
         end
      end
      -- fill (note: surface can be below bed because of evaporation)
      for y = y_bed+1, y_surface do
         pos.y = y
         data[area:indexp(pos)] = fill_id
      end
      -- use heightmap to fill the bed
      pos.y = heightmap[i]
      if y_surface <= y_bed then
         data[area:indexp(pos)] = c.mud -- [todo]
      else
         data[area:indexp(pos)] = bottom
      end
   end
end


-- generate_shores
--
-- debug only (for now, could add a shore_node setting)
--
local function generate_shores (shores)
   -- local c = get_content()
   -- for _, node in pairs(shores) do
   --    local nid = c.shores[node.dist] or c.shore0
   --    data[area:index(node.x, node.y, node.z)] = nid
   -- end
end


local function pick_in_list (list)
   if type(list) == "table" then
      assert(#list > 0) -- return nil?
      if #list == 1 then
         return list[1]
      else
         return list[math.random(#list)]
      end
   else
      return list
   end
end


-- place_decoration
--
local function place_decoration (pos, deco)
   -- don't place in air (can happen if there's a cavern below)
   local c = get_content()
   if data[area:index(pos.x, pos.y-1, pos.z)] == c.air then
      return
   end
   if deco.type == "simple" then
      local id = pick_in_list(deco.decoration)
      replace_node(pos, id)
   elseif deco.type == "schematic" then

      -- [fixme] is it a problem for us? Warning: Once you have loaded
      -- a schematic from a file, it will be cached. Future calls will
      -- always use the cached version and the replacement list
      -- defined for it, regardless of whether the file or the
      -- replacement list parameter have changed.

      -- LOGGER:debug("place schematic: %s", deco.schematic)
      -- local r = minetest.place_schematic_on_vmanip(vm,
      --                                              pos,
      --                                              deco.schematic,
      --                                              (deco.rotation or "random"),
      --                                              (deco.replacements or {}),
      --                                              (deco.force_placement or true),
      --                                              (deco.flags))
      pos = {x=pos.x, y=pos.y+(deco.place_offset_y or 0), z=pos.z}
      table.insert(schematics_list,{pos,
                                    deco.schematic,
                                    (deco.rotation or "random"),
                                    (deco.replacements or {}),
                                    (deco.force_placement or true),
                                    (deco.flags)})
      -- LOGGER:debug(" => %s", tostring(r))
   else
      LOGGER:error("invalid deco type: %s", tostring(deco.type))
   end
end


-- generate_decorations
--
local function generate_decorations (fillmap, shores)
   local decos = env.shore_decorations
   for _, node in pairs(shores) do
      if node then
         local n = math.random(100)
         local r
         if n < 75 then r = 1
         elseif n < 85 then r = 2
         elseif n < 90 then r = 3
         elseif n < 91 then r = 4
         end
         if r and #decos[r] > 0 then
            local pos = {x=node.x, y=node.y+1, z=node.z}
            place_decoration(pos, pick_in_list(decos[r]))
         end               
      end
   end

   -- [fixme] doing that during the 'fill' stage could save some time 
   decos = env.surface_decorations
   if #decos > 0 then
      for index, node in pairs(fillmap) do
         local y_fill = node.y
         local y_surface = y_fill - env.evaporate
         local y_bed = heightmap[index]
         if y_surface > y_bed then
            if  math.random(20) == 1 then
               local pos = {x=node.x, y=y_surface+1, z=node.z}
               place_decoration(pos, pick_in_list(decos))
            end
         end
      end
   end
end


local function place_debug_sign (volumes)
   for _, v in pairs(volumes) do
      local n = v.bases[1].nodes[1]
      local pos = {x=n.x, y=n.y+10, z=n.z}
      biome_tools.place_sign(pos,
                             string.format("%s\nHU:%d HE:%d\nBASES: %d\nDEPTH: %d\nSURF: %d\nCAPA: %d",
                                           minetest.get_biome_name(env.biome_id),
                                           env.humidity, env.heat,
                                           #v.bases, v.depth, v.surface, v.capacity))
      pos.x = pos.x+1
      biome_tools.place_sign(pos,
                             string.format("chance: %d\nmxdepth: %d\nmxcap: %d\nshore: %s",
                                           env.chance,
                                           env.max_depth,
                                           env.max_capacity,
                                           v.shore_size))
   end
end


-- on_generated
--
local function on_generated (minp_, maxp_, seed) -- luacheck: no unused args
   puddles._init()
   
   minp = minp_
   maxp = maxp_
   chulen = maxp.x - minp.x + 1

   env = get_environment_info(minp, maxp)
   LOGGER:debug("on_generated: %s - %s (chulen=%d)",
             P2S(minp), P2S(maxp), chulen)
   LOGGER:debug("environment :")
   LOGGER:debug(" - region: %s, humidity: %d, heat: %d",
                (env.region and env.region.name or "none"),
                env.humidity,
                env.heat)
   LOGGER:debug(" - chance: %d", env.chance)
   if not env.region then
      LOGGER:debug("no region found, gen skipped")
      return
   end

   -- heightmap (should go to env)
   heightmap = minetest.get_mapgen_object("heightmap")
   if not heightmap then
      log("no heightmap!")
      return
   end

   schematics_list = {}
   after_destruct_handlers = {}
   local emin, emax
   vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
   vm:get_data(data)
   vm:get_param2_data(param2_data)
   area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
   -- local vi = area:index(x, ground_y, z)

   if ENABLE_DEBUG then
      if ENABLE_DEBUG_HEIGHTMAP then
         debug_heightmap()
      end
      if ENABLE_DEBUG_CHUNKS then
         debug_chunks()
      end
   end

   local gen = {}
   local bases = detect_all_bases(gen)
   local volumes, fillmap
   if #bases > 0 then
      LOGGER:debug("found %d bases!", #bases)
      -- if ENABLE_DEBUG then
      --    debug_mark_bases(bases)
      -- end
      volumes, fillmap = detect_all_volumes(gen)
      local shores = detect_shores(gen)
      fill_all_volumes(volumes, fillmap)
      generate_shores(shores)
      generate_decorations(fillmap, shores)
   end

   vm:set_data(data)
   -- we only read param2 for now, no modif
   -- vm:set_param2_data(param2_data)
   vm:set_lighting({day=0, night=0})
   vm:calc_lighting()
   vm:update_liquids()
   vm:write_to_map()

   -- after destruct
   for _, h in ipairs(after_destruct_handlers) do
      h[1](h[2], h[3])
   end
   after_destruct_handlers = nil

   -- [fixme] schematics
   for _, s in ipairs(schematics_list) do
      -- table.insert(s, 1, vm)
      -- minetest.place_schematic_on_vmanip(unpack(s))
      minetest.place_schematic(unpack(s))
   end
   schematics_list = nil

   -- debug sign
   if volumes and ENABLE_DEBUG_SIGNS and biome_tools then
      place_debug_sign(volumes)
   end
end

minetest.register_on_generated(on_generated)
