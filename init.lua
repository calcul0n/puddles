--

local MODNAME = minetest.get_current_modname()
local MODPATH = minetest.get_modpath(MODNAME)

puddles = {
   mtt = dofile(MODPATH.."/mttools/init.lua"),
}
local LOGGER = puddles.mtt.logger
LOGGER:config({mtlevels={debug="action"}})

dofile(MODPATH.."/region.lua")
dofile(MODPATH.."/mapgen.lua")
dofile(MODPATH.."/api.lua")
