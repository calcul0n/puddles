-- AUTO-GENERATED - DO NOT HAND EDIT --

-- mttools/logging
--

local STRF = string.format

local Logger = {}

function Logger.new (name)
   assert(name, "logger name required: "..tostring(name))
   local logger = {
      name = name,
      prefix = STRF("[%s] ", name:upper()),
      mtlevels = {debug="none"},
   }
   setmetatable(logger, {__index=Logger})
   return logger
end

function Logger:config (config)
   self.mtlevels = config.mtlevels
end

function Logger:log (level, ...)
   minetest.log((self.mtlevels[level] or level), self.prefix..STRF(...))
end

function Logger:debug (...)
   self:log("debug", ...)
end

function Logger:info (levl, ...)
   self:log("info", ...)
end

function Logger:action (...)
   self:log("action", ...)
end

function Logger:warning (...)
   self:log("warning", ...)
end

function Logger:error (...)
   self:log("error", ...)
end

function Logger:fatal (...)
   local msg = self:log("error", ...)
   error(STRF(...))
end

local function init (ns)
   ns.logger = Logger.new(minetest.get_current_modname())
end

return init
