-- AUTO-GENERATED - DO NOT HAND EDIT --


local MODNAME = minetest.get_current_modname()
local MODPATH = minetest.get_modpath(MODNAME)


minetest.log("action", "[MTTOOLS] loading "..MODNAME.. " units")

local units = {}
local mttools = {units=units}

for _, name in ipairs(minetest.get_dir_list(MODPATH.."/mttools", false)) do
   local base = name:gsub("([^%.]*)%.(.*)", "%1")
   local ext  = name:gsub("([^%.]*)%.(.*)", "%2")
   if ext == "lua" and base ~= "init" then
      local path = MODPATH.."/mttools/"..name
      minetest.log("action", "[MTTOOLS] >> "..name.." ("..path..")")
      local init_func = dofile(path)
      init_func(mttools)
      units[base] = path
   end
end

return mttools
