-- AUTO-GENERATED - DO NOT HAND EDIT --

-- mttools/dump
--

local STRF = string.format


-- ldump
--
local function ldump (obj, memo)
   if type(obj) == "table" then
      memo = memo or {}
      if memo[obj] then
         return "{...}"
      end
      memo[obj] = true
      local keys = {}
      local islist = true
      for k, v in pairs(obj) do
         if not (type(k) == "number" and k >= 1 and k <= #obj) then
            islist = false
         end
         table.insert(keys, k)
      end
      local t = {}
      if islist then
         for i = 1, #obj do
            table.insert(t, ldump(obj[i]))
         end
         return "["..table.concat(t, ", ").."]"
      else
         table.sort(keys)
         for _, k in ipairs(keys) do
            table.insert(t, ldump(k).."="..ldump(obj[k]))
         end
         return "{"..table.concat(t, ", ").."}"
      end
   elseif type(obj) == "string" then
      return "\""..obj.."\""
   else
      return tostring(obj)
   end
end


-- dump_table
--
local function dump_table (title, tbl, concat)
   local lines = {title}
   local keys = {}
   local klen = 0
   for k, v in pairs(tbl) do
      table.insert(keys, k)
      klen = math.max(klen, #tostring(k))
   end
   local fmt = "  %-"..klen.."s = %s"
   table.sort(keys)
   for _, k in ipairs(keys) do
      table.insert(lines, STRF(fmt, tostring(k), ldump(tbl[k])))
   end
   -- note: default (nil) is true
   if concat ~= false then
      return table.concat(lines, (type(concat) == "string" and concat or "\n"))
   else
      return lines
   end
end


local function init (ns)
   ns.ldump = ldump
   ns.dump_table = dump_table
end


return init
