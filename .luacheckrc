-- -*- lua -*-

quiet = 1
codes = true

-- globals
globals = {
   puddles = {
      fields = {
         "settings",
         "registered_regions",
         "default_remove_nodes",
         "default_forbidden_content",
         "default_forbidden_grounds",
         "register_region",
         "_init_region",
      },
   },
}
read_globals = {
   "minetest",
   "dump",
   table = {
      fields = {
         "copy",
      },
   },
   vector = {
      fields = {
      },
   },
   VoxelArea = {
      fields = {
         "new",
      },
   },
   -- debug only
   dye = {
      fields = {"dyes"},
   },
}
std = "max"
